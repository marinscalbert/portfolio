function onSwitchThemeClicked(switchButton){
  let elementsToChange = document.querySelectorAll('[data-theme]');
  if (switchButton.checked){
    // Apply dark theme
    for (let i=0; i < elementsToChange.length; i++){
      let elementToChange = elementsToChange[i];
      elementToChange.setAttribute("data-theme", "dark");
    }
  }
  else {
    // Apply light theme
    for (let i=0; i < elementsToChange.length; i++){
      let elementToChange = elementsToChange[i];
      elementToChange.setAttribute("data-theme", "light");
    }
  }
}

function onAnchorClicked(){
  let burgerButton = document.getElementById('burger-button');
  let navContentToCollapse = document.getElementById("navbar-content-to-collapse")
  burgerButton.classList.toggle("collapsed");
  navContentToCollapse.classList.remove("show");
}
