let sections = document.getElementsByClassName("menu-section");


function highLightMenuItemOnScroll(){
  let windowScrollTopPosition = window.scrollY;
  let sectionsScrollRange = [];
  let scrollOffset = 60;
  for (let i=0; i <= sections.length-1; i++){
    let navLink = document.querySelector(`.nav-link[href='#${sections[i].id}']`);
    let topSection = sections[i].offsetTop;
    let bottomSection = sections[i].offsetTop+sections[i].offsetHeight;
    if (topSection <= windowScrollTopPosition+scrollOffset && windowScrollTopPosition+scrollOffset < bottomSection){
      // Add active class to nav-link with href = id
      navLink.classList.add("highlight");
    }
    else {
      // Remove active class to nav-link
      navLink.classList.remove("highlight");
    }
  };
}

window.addEventListener('scroll', highLightMenuItemOnScroll);
